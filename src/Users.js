import React, { useState, useEffect } from "react";
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Typography from '@mui/material/Typography';
import { Container } from '@mui/material';

export default function BasicGrid() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch("https://react-api-cso.herokuapp.com/orders")
      .then(res => res.json())
      .then(
        (result) => {
          setOrders(result);
        }
      )
  }, [])

  return (
    <Container maxWidth="lg" sx={{ mt: 2 }}>
      <Grid container spacing={2}>
        {orders.map(order => (
          <Grid item xs={12} md={4}>
            <Card>
              <CardHeader
                avatar={
                  <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                    {order.name[0]}
                  </Avatar>
                }
                action={
                  <IconButton aria-label="settings">
                    <MoreVertIcon />
                  </IconButton>
                }
                title={order.name + '[' + order.department + ']'}
              />
              <Typography variant="body2" ml={3}>
                 <b>ชนิด:</b> {order.type}
                 <br/>
                 <b>อาการ:</b> {order.problem}
                 <b>เบอร์โทร:</b> {order.tel}
              </Typography>
              <CardActions disableSpacing>
                <IconButton aria-label="add to favorites">
                  <FavoriteIcon />
                </IconButton>
                <IconButton aria-label="share">
                  <ShareIcon />
                </IconButton>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}